from django.db import models
from ckeditor.fields import RichTextField
from sorl.thumbnail import ImageField


# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(max_length=500)

    def __str__(self):
        return self.title


class Post(models.Model):
    title = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now=True)
    content = RichTextField()
    featured_image = ImageField()
    category = models.ForeignKey(Category(title), on_delete=models.CASCADE)

    def __str__(self):
        return self.title
