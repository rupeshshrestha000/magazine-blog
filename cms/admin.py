from django.contrib import admin
from .models import Menu, Service, Testimonial, Team, Client

# Register your models here.

admin.site.register(Menu)


# ========================================================================
class ServiceDescription(admin.ModelAdmin):
    list_display = ('title', 'description')

    def services_info(self, obj):
        return obj.description


admin.site.register(Service, ServiceDescription)


# ==========================================================================

class TestimonialDescription(admin.ModelAdmin):
    list_display = ('name', 'designation', 'message')

    def testimonial_info(self, obj):
        return obj.description


admin.site.register(Testimonial, TestimonialDescription)


# ====================================================================

class TeamDescription(admin.ModelAdmin):
    list_display = ('fullname', 'designation')

    def team_info(self, obj):
        return obj.description


admin.site.register(Team, TeamDescription)
# ============================================================
admin.site.register(Client)
