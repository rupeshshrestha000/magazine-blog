from django.shortcuts import render
from .models import Menu, Service, Testimonial, Team, Client


# Create your views here.

def index(request):
    context = {
        'menu': Menu.objects.all(),
        'services': Service.objects.all(),
        'testimonial': Testimonial.objects.all(),
        'team': Team.objects.all(),
        'client': Client.objects.all()
    }
    return render(request, 'index.html', context)
