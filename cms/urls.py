from django.urls import path
from .views import index

app_name = 'cms'

urlpatterns = [
    path('', index, name='home-page'),

]
