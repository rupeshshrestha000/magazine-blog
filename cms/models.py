from django.db import models
from ckeditor.fields import RichTextField
from sorl.thumbnail import ImageField


# Create your models here.
class Menu(models.Model):
    menu = models.CharField(max_length=25)

    def __str__(self):
        return self.menu


class Service(models.Model):
    title = models.CharField(max_length=500)
    description = RichTextField()

    def __str__(self):
        return self.title


class Testimonial(models.Model):
    name = models.CharField(max_length=100)
    designation = models.CharField(max_length=100)
    message = RichTextField()
    image = ImageField()

    def __str__(self):
        return self.name


class Team(models.Model):
    image = ImageField()
    fullname = models.CharField(max_length=100)
    designation = models.CharField(max_length=100)
    facebook = models.CharField(max_length=100)
    twitter = models.CharField(max_length=100)
    linkedin = models.CharField(max_length=100)

    def __str__(self):
        return self.fullname


class Client(models.Model):
    client_name = models.CharField(max_length=100)
    logo = ImageField()

    def __str__(self):
        return self.client_name
